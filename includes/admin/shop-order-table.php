<?php

add_filter( 'manage_edit-shop_order_columns', 'wc_add_custom_shop_order_column');

function wc_add_custom_shop_order_column( $columns ) {
  $columns['api_document_type'] = __( 'Tipo de documento', 'woocommerce' );
  $columns['_ep_sunat_api_state_type_description'] = __( 'Estado en API/SUNAT', 'woocommerce' );
  return $columns;
}

add_action( 'manage_shop_order_posts_custom_column', 'tk_add_order_api_document_type' );
function tk_add_order_api_document_type($column) {
  global $post;

  $order_id = $post->ID;
  $order = wc_get_order( $order_id );
  $api_document_type = get_post_meta( $post->ID, 'api_document_type', true );
  $api_document_number = get_post_meta( $post->ID, '_ep_sunat_api_number', true );
  $api_response = get_post_meta( $post->ID, '_ep_sunat_api_state_type_description', true );

  if( $column == 'api_document_type' ) {
    echo $api_document_type ? "<span style='text-transform:capitalize;'>".$api_document_type."</span><p>".$api_document_number."</p>" : "N/A";
  }

  if($column == '_ep_sunat_api_state_type_description') {
    echo $api_response ? "<span style='text-transform:uppercase;'>".$api_response."</span>" : "N/A";
  }
}